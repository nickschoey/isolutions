import React from 'react';
import PropTypes from 'prop-types';
import './Item.css';
import Emoji from './Emoji';
const Item = ({ width, height, title, top, left }) => {
  return (
    <div
      className={`item ${title}`}
      style={{
        height: height,
        maxHeight: height,
        minHeight: height,
        width: width,
        maxWidth: width,
        minWidth: width,
        top: top,
        left: left
      }}
    >
      {(() => {
        switch (title) {
          case 'Tomato':
            return <Emoji symbol="🍅" label="tomato" />;
          case 'Veal':
            return <Emoji symbol="🍗" label="Veal" />;
          case 'Steak':
            return <Emoji symbol="🍖" label="Steak" />;
          case 'Sausage':
            return <Emoji symbol="🌭" label="Sausage" />;
          default:
            return <div />;
        }
      })()}
    </div>
  );
};

Item.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  top: PropTypes.number,
  left: PropTypes.number,
  count: PropTypes.number,
  title: PropTypes.string
};

export default Item;
