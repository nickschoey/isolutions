import React from 'react';
import Item from '../_components/Item';


// function to represent the elements on the grill displayed on the browser adjusted to the desired factor to prevent the grill from being too small. This is separarted from the rest since it deals with React Components.

// TODO: Maybe should be treated as a component itself?
export const placeItems = (populatedGrill, factor) => {
  return populatedGrill.map((el, i) => {
    if (el.fit) {
      return (
        <Item
          key={i}
          height={el.height * factor}
          width={el.width * factor}
          title={el.title}
          top={el.fit.y * factor}
          left={el.fit.x * factor}
        />
      );
    }
  });
};
