import React from 'react';
import './Grill.css';
import PropTypes from 'prop-types';

const Grill = ({ dimensions, factor, children }) => (
  <div
    className="grill"
    style={{
      width: dimensions.width * factor,
      height: dimensions.height * factor,
    }}
  >
   {children}
  </div>
);

Grill.propTypes = {
  children: PropTypes.array,
  dimensions: PropTypes.object,
  factor: PropTypes.number
};

export default Grill;
