import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Grill from './_components/Grill';
import Stock from './_components/Stock';
import './App.css';
import {
  populateGrill,
  calculateRealState,
  remainingStock
} from './_helpers/myFunctions';
import { placeItems } from './_helpers/placeItems';
import Options from './_components/Options';
import Surface from './_components/Surface';

export class App extends Component {
  constructor() {
    super();
    this.state = {
      placedElements: [],
      realState: 0,
      sort: 'area',
      remainingStock: []
    };
  }
  // On main component mount the remainingStock gets set to the stock from the JSON file.

  componentDidMount = () => {
    const { initialStock } = this.props;
    this.setState({ remainingStock: initialStock });
  };

  // This is the main functionality of the App, which runs the algorithm, sorts the list of items and represents it with the proper scale factor in the browser grid. The surface covered gets also updated. See _helpers/ for the main functions running this. 
  placeItem = () => {
    const { grillDimensions, initialStock, factor } = this.props;
    const { sort } = this.state;

    const populatedGrill = populateGrill(grillDimensions, initialStock, sort);

    this.setState({
      realState: calculateRealState(populatedGrill, grillDimensions),
      remainingStock: remainingStock(populatedGrill),
      placedElements: placeItems(populatedGrill, factor)
    });
  };

  // Resets everything and sorts the stock depending on the option chosen.
  handleMethodChange = e => {
    const { initialStock } = this.props;
    this.setState({
      sort: e.target.value,
      placedElements: [],
      remainingStock: initialStock,
      realState: 0
    });
  };

  render() {
    const { grillDimensions, factor } = this.props;
    const { sort, placedElements, realState, remainingStock } = this.state;
    return (
      <div className="App">
        <div className="title">
          <h2>iSolutions GrillMaster UI</h2>
        </div>
        <div className="top_row">
          <Surface realState={realState} />
          <Grill
            dimensions={grillDimensions}
            factor={factor}
            surfaceCovered={realState}
          >
            {placedElements}
          </Grill>
          <Stock grillItems={remainingStock} factor={factor} />
        </div>
        <Options
          sort={sort}
          placeItem={this.placeItem}
          handleMethodChange={this.handleMethodChange}
        />
      </div>
    );
  }
}

App.propTypes = {
  sortStock: PropTypes.func,
  initialStock: PropTypes.array,
  grillDimensions: PropTypes.object,
  factor: PropTypes.number,
  grillItems: PropTypes.array
};

//Factor, grillDimensions and initialStock come from the redux store, which, for now, is only used as a source of truth. See the grill reducer for more info on this.

const mapStateToProps = state => ({
  factor: state.factor,
  grillDimensions: { width: state.grill.width, height: state.grill.height },
  initialStock: state.grill.initialStock
});

// const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  null
)(App);
