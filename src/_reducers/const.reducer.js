const defaultState = 40;

//Reducer that specifies the factor to multiply every grill or item size so it scales enough to be represented on the browser. As it is now it's not very useful but it has the potential to be modified and prevents the usage of a magic number.

export default (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
