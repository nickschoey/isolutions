import React from 'react'
import PropTypes from 'prop-types';
import './Options.css';

const Options = ({sort, placeItem, handleMethodChange}) => {
  return <div className="options">
      <div>
        <select className="options_select" value={sort} onChange={handleMethodChange}>
          <option value="area">Area</option>
          <option value="width">Width</option>
          <option value="height">Height</option>
          <option value="side">Side</option>
          <option value="random">Random</option>
          <option value="reverseArea">Smallest area</option>
        </select>
      </div>
      <div className="options_place">
        <span onClick={placeItem} className="place_button">
          Place
        </span>
      </div>
      <div className="options_third">
      <a href="https://gitlab.com/nickschoey/isolutions" target="_blank">
          <span className="place_button">GitLab</span>
        </a>
      </div>
    </div>;
}
Options.propTypes = {
  sort: PropTypes.string,
  placeItem: PropTypes.func,
  handleMethodChange: PropTypes.func
};

export default Options
