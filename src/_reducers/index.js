import { combineReducers } from "redux";
import grill from './grill.reducer';
import factor from './const.reducer';
const reducers = combineReducers({
  grill,
  factor
})

export default reducers;