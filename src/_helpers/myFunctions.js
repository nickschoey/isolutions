import { Packer } from './packer';

//This file implements different functions used for the algorithm to run properly.


// Creates an array of items to be placed on the grill by unfolding the initialStock object. This is a helper for sortList.
const createItemsList = itemsObject => {
  return itemsObject.reduce((acc, el) => {
    for (let index = 0; index < el.count; index++) {
      acc.push({ ...el });
    }
    return acc;
  }, []);
};

// Sorts the array based on the sortingMethod selected by the user. This is a helper for populateGrill.
const sortList = (itemsObject, sortingMethod) => {
  const itemsArray = createItemsList(itemsObject);
  switch (sortingMethod) {
    case 'area':
      return itemsArray.sort((a, b) => b.height * b.width - a.height * a.width);
    case 'random':
      return itemsArray.sort(() => 0.5 - Math.random());
    case 'side':
      return itemsArray.sort((a, b) => (b.height + b.width) - (a.height + a.width));
    case 'width':
      return itemsArray.sort((a, b) => b.width - a.width);
    case 'height':
      return itemsArray.sort((a, b) => b.height - a.height);
    case 'reverseArea':
      return itemsArray
        .sort((a, b) => b.height * b.width - a.height * a.width)
        .reverse();
    default:
      return itemsArray;
  }
};

// Creates the sortedList and a new packer and runs the algorithm.
export const populateGrill = (grillDimensions, itemsObject, sortingMethod) => {
  const sortedList = sortList(itemsObject, sortingMethod);
  const packer = new Packer(grillDimensions.width, grillDimensions.height);
  packer.fit(sortedList);
  return sortedList;
};


// Updates the surface covered by the items on the grill.
export const calculateRealState = (itemsArray, grillDimensions) => {
  const grillArea = grillDimensions.width * grillDimensions.height;
  const areaOccupied = itemsArray.reduce((acc, el) => {
    if (el.fit) {
      acc += el.width * el.height;
    }

    return acc;
  }, 0);
  return Math.round((areaOccupied / grillArea) * 100);
};


// Updates the stock that has not been able to put on the grill.
export const remainingStock = itemsArray => {
  return itemsArray.reduce((acc, el) => {
    if (!el.fit) {
      if (!acc[el.title]) {
        acc[el.title] = {
          title: el.title,
          count: 1,
          width: el.width,
          height: el.height
        };
      } else acc[el.title].count++;
    }
    return acc;
  }, {});
};
