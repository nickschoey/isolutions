// This is the main algorithm used to place the elements on the grill. It basically takes the first element from the list and tries to place it in the first slot available. Then it calculates the space to the right and the space to the bottom and creates a binary tree that is travelled by the next item in turn until it runs out of space. Every object in the array gets a node that specifies wether it fits or not, the exact position on the grill and the right and bottom node surrounding the item.

// Unfortunately, this is not solving the problem perfecty: It is not trying to place the items vertically nor is able to 'see' other available spaces other than the ones spotted on the binary tree.

// This is an implementation of the bin packing binary tree algorithm by Jake Gordon https://codeincomplete.com/posts/bin-packing/ which in turn is an implementation of the paper by Jim Scott on packing lightmaps: http://blackpawn.com/texts/lightmaps/default.html.



export class Packer {
  constructor(w, h) {
    this.root = { x: 0, y: 0, w: w, h: h };
  }

  fit(blocks) {
    let n, node, block;
    for (n = 0; n < blocks.length; n++) {
      block = blocks[n];
      if ((node = this.findNode(this.root, block.width, block.height)))
        block.fit = this.splitNode(node, block.width, block.height);
    }
  }

  findNode(root, w, h) {
    if (root.used)
      return this.findNode(root.right, w, h) || this.findNode(root.down, w, h);
    else if (w <= root.w && h <= root.h) return root;
    else return null;
  }

  splitNode(node, w, h) {
    node.used = true;
    node.down = { x: node.x, y: node.y + h, w: node.w, h: node.h - h };
    node.right = { x: node.x + w, y: node.y, w: node.w - w, h: h };
    return node;
  }
}
