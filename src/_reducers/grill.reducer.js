import data from '../grill.json';

// The redux reducers in this exercise are used as a 'source of truth' across the app (like a database). Most of the logic will happen in the main App component itself while the rest of the components will work as 'dumb' components.

// As it is now it doesn't do anything other than serving as a database but it has been structured this way so it has future capability to modify the size of the grid and the initial stock through dispatching actions.

const defaultState = {
  width: data.grill.width,
  height: data.grill.height,
  initialStock: data.grill.grillitems.grillitem,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
