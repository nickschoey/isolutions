import React from 'react'
import PropTypes from 'prop-types';
import './Surface.css';

const Surface = ({realState}) => {
  return (
    <div className="surface">
      <div>Surface Covered:</div>
      <div className="amount">{realState} %</div>
    </div>
  )
}
Surface.propTypes = {
  realState: PropTypes.number,
}
export default Surface
