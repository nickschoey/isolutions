import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';
import './Stock.css'
const Stock = ({grillItems, factor }) => {
  return (
    <div className='stock'>
        {Object.values(grillItems).map((el, i) => (
          <div className="stockItemGroup"
          key={el.title}
          >
          <div className="stockItemGroupAmount">
            {el.count}x
          </div>
          <Item
            width={el.width * factor}
            height={el.height * factor}
            count={el.count}
            title={el.title}
            index={i}
          />
          </div>
        ))}
    </div>
  );
};

Stock.propTypes = {
  grillItems: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  factor: PropTypes.number,
};

export default Stock;
